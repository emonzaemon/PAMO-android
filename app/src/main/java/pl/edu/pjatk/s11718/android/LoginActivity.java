package pl.edu.pjatk.s11718.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import pl.edu.pjatk.s11718.android.models.User;

public class LoginActivity extends AbstractActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private EditText emailField;
    private EditText passwordField;
    private Button loginButton;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        initViewFields();
        setListeners();
    }

    private void initViewFields() {
        emailField = (EditText) findViewById(R.id.email_field);
        passwordField = (EditText) findViewById(R.id.password_field);
        loginButton = (Button) findViewById(R.id.login_button);
        registerButton = (Button) findViewById(R.id.register_button);
    }

    private void setListeners() {
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
    }

    private void login(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, createOnCompleteListener());
    }

    private void register(String email, String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, createOnCompleteListener());
    }

    private OnCompleteListener createOnCompleteListener() {
        return new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                hideLoading();
                if (task.isSuccessful()) {
                    onAuth(task.getResult().getUser());
                } else {
                    Toast.makeText(LoginActivity.this, "Błąd. Sprawdź dane", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        if (!validate()) {
            return;
        }
        showLoading();
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();
        if (R.id.login_button == view.getId()) {
            login(email, password);
        } else if (R.id.register_button == view.getId()){
            register(email, password);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (null != firebaseAuth.getCurrentUser()) {
            onAuth(firebaseAuth.getCurrentUser());
        }
    }

    private void onAuth(FirebaseUser firebaseUser) {
        createUser(firebaseUser.getUid(), firebaseUser.getEmail());
        startActivity(new Intent(LoginActivity.this, StartupActivity.class));
        finish();
    }

    private void createUser(String id, String email) {
        databaseReference.child("users").child(id).setValue(new User(email));
    }

    private boolean validate() {
        boolean result = true;
        result = checkIfFieldEmpty(result, emailField);
        result = checkIfFieldEmpty(result, passwordField);
        return result;
    }

    private boolean checkIfFieldEmpty(boolean result, EditText field) {
        if (TextUtils.isEmpty(field.getText().toString())) {
            field.setError("Pole wymagane!");
            result = false;
        } else {
            field.setError(null);
        }
        return result;
    }
}