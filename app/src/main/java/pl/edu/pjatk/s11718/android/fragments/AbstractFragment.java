package pl.edu.pjatk.s11718.android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import pl.edu.pjatk.s11718.android.R;
import pl.edu.pjatk.s11718.android.models.Game;
import pl.edu.pjatk.s11718.android.views.ItemViewHolder;

public abstract class AbstractFragment extends Fragment {

    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Game, ItemViewHolder> firebaseRecyclerAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    public AbstractFragment() {}

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_item_list, container, false);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.items_list);
        recyclerView.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.cleanup();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        Query itemsQuery = getQuery(databaseReference);
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Game, ItemViewHolder>(
                Game.class,
                R.layout.fav_row,
                ItemViewHolder.class,
                itemsQuery
        ) {
            @Override
            protected void populateViewHolder(
                    final ItemViewHolder viewHolder,
                    final Game model,
                    final int position
            ) {
                viewHolder.bindToItem(model);
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public String getUID() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public abstract Query getQuery(DatabaseReference databaseReference);
}
