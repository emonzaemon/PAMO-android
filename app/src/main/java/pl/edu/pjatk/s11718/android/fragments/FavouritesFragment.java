package pl.edu.pjatk.s11718.android.fragments;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class FavouritesFragment extends AbstractFragment {

    private final int queryLimit = 150;

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("user-games")
                .child(getUID()).orderByChild("name").limitToFirst(queryLimit);
    }

}
