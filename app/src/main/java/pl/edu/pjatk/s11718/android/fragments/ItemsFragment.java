package pl.edu.pjatk.s11718.android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.pjatk.s11718.android.R;
import pl.edu.pjatk.s11718.android.models.Game;
import pl.edu.pjatk.s11718.android.views.MyAdapter;

public class ItemsFragment extends Fragment {

    private String urlGames = "https://igdbcom-internet-game-database-v1.p.mashape.com/games/?fields=name,summary,popularity&limit=50&offset=0&order=release_dates.date%3Adesc";
    private RequestQueue requestQueue;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager linearLayoutManager;
    private List<Game> gameList = new ArrayList<>();


    public ItemsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity());
        makeItemsRequest();
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_item_list, container, false);
        View itemRow = inflater.inflate(R.layout.item_row, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.items_list);
        recyclerView.setHasFixedSize(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        mAdapter = new MyAdapter(gameList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }


    private void makeItemsRequest() {

        StringRequest stringRequest = new StringRequest
                (Request.Method.GET, urlGames,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {

                                Gson gson = new Gson();
                                Type GameType = new TypeToken<Collection<Game>>() {}.getType();
                                List<Game> games = gson.fromJson(response, GameType);
                                for (Game game : games) {
                                    gameList.add(game);
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
        {

            @Override
        public Map<String, String> getHeaders() {
            Map headers = new HashMap();
            headers.put("X-Mashape-Key", "DLPFxCF7UBmshHX1vsEGJUJUr3OHp10RwYmjsnJP42vgpG5YDb");
            headers.put("Accept", "application/json");
            return headers;
        }};
      requestQueue.add(stringRequest);
    }

}

