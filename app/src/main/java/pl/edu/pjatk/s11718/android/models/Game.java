package pl.edu.pjatk.s11718.android.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game {

    public String uid;
    public int id;
    public String name;
    public String summary;
    public Double popularity;
    List<Game> games;

    public Game(String uid, String name, String summary, Double popularity) {
        this.uid = uid;
        this.name = name;
        this.summary = summary;
        this.popularity = popularity;
    }

    public Game() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getPopularity() {
        this.popularity = Math.round(popularity * 10.0) / 10.0;
        return Double.toString(this.popularity);
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String,Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("name", name);
        result.put("summary", summary);
        result.put("popularity", popularity);
        return result;
    }
}
