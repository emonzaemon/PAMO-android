package pl.edu.pjatk.s11718.android.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    private String username;
    private String email;

    public User() {}
    public User(String email) {
        this.username = prepareUsername(email);
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    private String prepareUsername(String email) {
        return email.contains("@") ? email.split("@")[0] : email;
    }
}