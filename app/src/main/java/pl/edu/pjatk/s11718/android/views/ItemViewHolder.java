package pl.edu.pjatk.s11718.android.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pl.edu.pjatk.s11718.android.R;
import pl.edu.pjatk.s11718.android.models.Game;

public class ItemViewHolder extends RecyclerView.ViewHolder {

    public TextView name;
    public TextView summary;
    public TextView popularity;

    public ItemViewHolder(View view) {
        super(view);

        name = (TextView) view.findViewById(R.id.game_name);
        summary = (TextView) view.findViewById(R.id.game_summary);
        popularity = (TextView) view.findViewById(R.id.game_popularity);
    }

    public void bindToItem(Game game) {
                name.setText(game.getName());
                summary.setText(game.getSummary());
                popularity.setText("Popularność: " + game.getPopularity());
    }
}
