package pl.edu.pjatk.s11718.android.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.pjatk.s11718.android.R;
import pl.edu.pjatk.s11718.android.models.Game;
import pl.edu.pjatk.s11718.android.models.User;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private DatabaseReference databaseReference;
    private List<Game> gameList;


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView summary;
        public TextView popularity;

        public ViewHolder(View view) {
            super(view);


            final Button addButton = (Button) itemView.findViewById(R.id.add_btnn);
            addButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int position  =   getAdapterPosition();
                    favouriteButtonOnClick(position, v);
                }
            });

            name = (TextView) view.findViewById(R.id.game_name);
            summary = (TextView) view.findViewById(R.id.game_summary);
            popularity = (TextView) view.findViewById(R.id.game_popularity);
        }


        public String getIdentity() {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();
        }

        private void favouriteButtonOnClick(int position,  final View view) {
            final int pos = position;
            final String userId = getIdentity();
            databaseReference.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Game game = gameList.get(pos);
                    User user = dataSnapshot.getValue(User.class);
                    if (user == null) {
                    Toast.makeText(view.getContext(), "błąd przy dodawaniu", Toast.LENGTH_SHORT).show();
                    } else {
                        addFavourite(getIdentity(), game.getName(), game.getSummary(), game.popularity);
                        Toast.makeText(view.getContext(), "Dodano " + game.getName(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }

        private void addFavourite(String userId, String name, String summary, Double popularity) {
            String key = databaseReference.child("games").push().getKey();
            Game game = new Game(userId, name, summary, popularity);
            Map<String, Object> gameValues = game.toMap();
            Map<String, Object> updates = new HashMap<>();
            updates.put("/games/" + key, gameValues);
            updates.put("/user-games/" + userId + "/" + key, gameValues);
            databaseReference.updateChildren(updates);
        }

    }

    public MyAdapter(List<Game> gameList) {
        this.gameList = gameList;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        Game game = gameList.get(position);
        holder.name.setText(game.getName());
        holder.summary.setText(game.getSummary());
        holder.popularity.setText("Popularność: " + game.getPopularity());
    }


    public int getItemCount() {
        return gameList.size();
    }


}